# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase


class SaleSupplyStateTestCase(ModuleTestCase):
    "Test Sale Supply State module"
    module = 'sale_supply_state'


del ModuleTestCase
